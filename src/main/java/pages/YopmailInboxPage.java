package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public class YopmailInboxPage {
    private WebDriver driver;
    private WebElement title;
    private Wait<WebDriver> wait;

    @FindBy(id = "refresh")
    private WebElement refreshButton;

    private By inboxIframe = By.xpath("//iframe[@id='ifinbox']");
    private By firstEmailTitle = By.xpath("//div[@class='lms']");

    public YopmailInboxPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(12))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class);
        //
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='ifnoinb']")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/h4")));
        driver.switchTo().defaultContent();
    }
    public String getFirstEmailTitle() throws InterruptedException {
        Thread.sleep(3000);
        refreshButton.click();
        driver.switchTo().defaultContent();

        wait.until(ExpectedConditions.presenceOfElementLocated(inboxIframe));
        driver.switchTo().frame(driver.findElement(inboxIframe));

        for (int i = 0; i < 2; i++) {
            try {
                title = wait.until(ExpectedConditions.presenceOfElementLocated(firstEmailTitle));
            }catch (TimeoutException e){
                System.out.println(e.getMessage());
                driver.switchTo().defaultContent();
                driver.switchTo().frame(driver.findElement(inboxIframe));
            }
        }

        System.out.println("text:"+title.getText());
        return title.getText();
    }
}
