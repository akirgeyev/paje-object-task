package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public class YopmailHomePage {
    private final WebDriver driver;
    private final Wait<WebDriver> wait;
    private static final String HOMEPAGE_URL = "https://yopmail.com/";

    //checkInbox
    private final By checkInboxButton = By.xpath("//div[@class='nw']/button[2]");
    //generateEmail
    private final By generateEmailLink = By.xpath("//div[@id='listeliens']/a[1]");
    //emailAddress
    private final By emailAddressText = By.xpath("//*[@id='geny']/span[1]");

    private final By consentButton = By.cssSelector("[aria-label='Consent']");


    public YopmailHomePage(WebDriver driver) {
        this.driver = driver;
        driver.get(HOMEPAGE_URL);
        wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class);
    }

    public String generateRandomEmail(){
        wait.until(ExpectedConditions.elementToBeClickable(consentButton)).click();
        wait.until(ExpectedConditions.elementToBeClickable(generateEmailLink)).click();

        try {
            driver.switchTo().defaultContent();

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='dismiss-button']"))).click();
        }catch (TimeoutException exception){
            System.out.println(exception.getMessage());
        }

        WebElement emailAddress = wait.until(
                ExpectedConditions.presenceOfElementLocated(emailAddressText));
        System.out.println("email:" + emailAddress.getText());
        return emailAddress.getText()+"@yopmail.com";
    }

    public YopmailInboxPage checkInbox(){
        driver.findElement(checkInboxButton).click();
        return new YopmailInboxPage(driver);
    }
}


