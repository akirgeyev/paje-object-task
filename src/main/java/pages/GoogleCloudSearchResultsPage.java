package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GoogleCloudSearchResultsPage {
    private final WebDriver driver;
    private final String term;

    public GoogleCloudSearchResultsPage(WebDriver driver, String term) {
        this.driver = driver;
        this.term = term;
        new WebDriverWait(driver, Duration.ofSeconds(5));
    }

    public void openLinkWithMatch() {
        String url = "https://cloudpricingcalculator.appspot.com/";
        driver.get(url);
    }

}
