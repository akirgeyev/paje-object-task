package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GoogleCloudHomePage {
    private static final String HOMEPAGE_URL = "https://cloud.google.com/";
    private WebDriver driver;

    @FindBy(xpath = "//div[@class = 'YSM5S']")
    private WebElement searchIcon;

    @FindBy(xpath = "//div[@class='YSM5S SkvQFd']/input[@class='mb2a7b']")
    private WebElement searchInput;

    public GoogleCloudHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public GoogleCloudHomePage openPage() {
        driver.get(HOMEPAGE_URL);
        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class = 'YSM5S']")));
        return this;
    }

    public GoogleCloudSearchResultsPage searchTerm(String term) {
        searchIcon.click();
        searchInput.sendKeys(term);
        searchInput.sendKeys(Keys.RETURN);
        return new GoogleCloudSearchResultsPage(driver, term);
    }
}
