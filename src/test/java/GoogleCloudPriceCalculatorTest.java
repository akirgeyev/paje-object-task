import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.GoogleCloudHomePage;
import pages.GoogleCloudPricingCalculatorPage;
import pages.GoogleCloudSearchResultsPage;
import pages.YopmailHomePage;

public class GoogleCloudPriceCalculatorTest {
    private WebDriver driver;
    private WebDriver yopmailDriver;

    @Before
    public void browserSetup() {
        driver = new ChromeDriver();
    }

    @Test
    public void testCloudPricingCalculate() throws InterruptedException {
        GoogleCloudHomePage homePage = new GoogleCloudHomePage(driver);
        homePage.openPage();
        GoogleCloudSearchResultsPage resultsPage = homePage.searchTerm("Google Cloud Pricing Calculator");
        resultsPage.openLinkWithMatch();
        GoogleCloudPricingCalculatorPage calculatorPage = new GoogleCloudPricingCalculatorPage(driver).setFields();

        yopmailDriver = new ChromeDriver();
        YopmailHomePage yopmailHomePage = new YopmailHomePage(yopmailDriver);

        String email = yopmailHomePage.generateRandomEmail();

        calculatorPage.sendEstimateEmail(email);

        String title = yopmailHomePage.checkInbox().getFirstEmailTitle();
        System.out.println("title:" + title);
        Assert.assertEquals("Google Cloud Price Estimate", title);
    }

    @After
    public void cleanUp() {
        if (driver != null) {
            driver.quit();
        }
        if (yopmailDriver != null) {
            yopmailDriver.quit();
        }
    }
}
