package steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.testng.Assert;
import pages.GoogleCloudHomePage;
import pages.GoogleCloudPricingCalculatorPage;
import pages.GoogleCloudSearchResultsPage;
import pages.YopmailHomePage;
import pages.YopmailInboxPage;

public class CloudPricingCalculatorSteps {
    private WebDriver driver;
    private WebDriver yopmailDriver;
    private GoogleCloudHomePage homePage;
    private GoogleCloudSearchResultsPage resultsPage;
    private GoogleCloudPricingCalculatorPage calculatorPage;
    private YopmailHomePage yopmailHomePage;
    private String email;

    @Before
    public void setup() {
        driver = new ChromeDriver();
    }

    @Given("I am on the Google Cloud Home page")
    public void openHomePage() {
        homePage = new GoogleCloudHomePage(driver);
        homePage.openPage();
    }

    @When("I search for {string}")
    public void searchTerm(String term) {
        resultsPage = homePage.searchTerm(term);
    }

    @When("I select the calculator from the search results")
    public void selectCalculator() {
        resultsPage.openLinkWithMatch();
        calculatorPage = new GoogleCloudPricingCalculatorPage(driver);
    }

    @When("I fill in the calculator form")
    public void fillInCalculatorForm() {
        calculatorPage.setFields();
    }

    @When("I generate a random email")
    public void generateRandomEmail() {
        yopmailDriver = new ChromeDriver();
        yopmailHomePage = new YopmailHomePage(yopmailDriver);
        email = yopmailHomePage.generateRandomEmail();
    }

    @When("I send the estimate to the email")
    public void sendEstimateToEmail() {
        calculatorPage.sendEstimateEmail(email);
    }

    @Then("I should receive an email with the estimate")
    public void verifyEmail() throws InterruptedException {
        YopmailInboxPage inboxPage = yopmailHomePage.checkInbox();
        String title = inboxPage.getFirstEmailTitle();
        Assert.assertEquals("Google Cloud Price Estimate", title);
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
        if (yopmailDriver != null) {
            yopmailDriver.quit();
        }
    }
}
