import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.qameta.allure.testng.AllureTestNg;
import org.testng.annotations.Listeners;

@Listeners({AllureTestNg.class})
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "steps"
)
public class TestRunner extends AbstractTestNGCucumberTests {
}
