Feature: Google Cloud Price Calculator

  Scenario: Calculate the price and verify email
    Given I am on the Google Cloud Home page
    When I search for "Google Cloud Pricing Calculator"
    And I select the calculator from the search results
    And I fill in the calculator form
    And I generate a random email
    And I send the estimate to the email
    Then I should receive an email with the estimate
